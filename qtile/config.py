# Copyright (c) 2010 Aldo Cortesi
# Copyright (c) 2010, 2014 dequis
# Copyright (c) 2012 Randall Ma
# Copyright (c) 2012-2014 Tycho Andersen
# Copyright (c) 2012 Craig Barnes
# Copyright (c) 2013 horsik
# Copyright (c) 2013 Tao Sauvage

#MY CONFIG based in DT's

#IMPORTS
import os
import re
import socket
import subprocess

from libqtile.config import Key, Screen, Group, Drag, Click
from libqtile.lazy import lazy
from libqtile import layout, bar, widget, hook

from typing import List  # noqa: F401

#VARIABLES
mod = "mod4"
myTerm = "alacritty"
myConfig = "/home/siwar/.config/qtile/config.py"

#KEYBINDINGS
keys = [
    ### The Essentials
    Key([mod], "Return",
        lazy.spawn(myTerm),
        desc='Launches My Terminal'
        ),
    Key([mod, "shift"], "Return",
        lazy.spawn("dmenu_run -p 'Run: '"),
        desc='Dmenu Run Launcher'
        ),
    Key([mod], "Tab",
        lazy.next_layout(),
        desc='Toggle through layouts'
        ),
    Key([mod, "shift"], "c",
        lazy.window.kill(),
        desc='Kill active window'
        ),
    Key([mod, "shift"], "r",
        lazy.restart(),
        desc='Restart Qtile'
        ),
    Key([mod, "shift"], "q",
        lazy.shutdown(),
        desc='Shutdown Qtile'
        ),

    ### Treetab controls
    Key([mod, "control"], "k",
        lazy.layout.section_up(),
        desc='Move up a section in treetab'
        ),
    Key([mod, "control"], "j",
        lazy.layout.section_down(),
        desc='Move down a section in treetab'
        ),

    ### Window controls
    Key([mod], "k",
        lazy.layout.down(),
        desc='Move focus down in current stack pane'
        ),
    Key([mod], "j",
        lazy.layout.up(),
        desc='Move focus up in current stack pane'
        ),
    Key([mod, "shift"], "k",
        lazy.layout.shuffle_down(),
        desc='Move windows down in current stack'
        ),
    Key([mod, "shift"], "j",
        lazy.layout.shuffle_up(),
        desc='Move windows up in current stack'
        ),
    Key([mod], "h",
        lazy.layout.grow(),
        lazy.layout.increase_nmaster(),
        desc='Expand window (MonadTall), increase number in master pane (Tile)'
        ),
    Key([mod], "l",
        lazy.layout.shrink(),
        lazy.layout.decrease_nmaster(),
        desc='Shrink window (MonadTall), decrease number in master pane (Tile)'
        ),
    Key([mod], "n",
        lazy.layout.normalize(),
        desc='normalize window size ratios'
        ),
    Key([mod], "m",
        lazy.layout.maximize(),
        desc='toggle window between minimum and maximum sizes'
        ),
    Key([mod, "shift"], "f",
        lazy.window.toggle_floating(),
        desc='toggle floating'
        ),
    Key([mod, "shift"], "m",
        lazy.window.toggle_fullscreen(),
        desc='toggle fullscreen'
        ),

    ### Stack controls
    Key([mod, "shift"], "space",
        lazy.layout.rotate(),
        lazy.layout.flip(),
        desc='Switch which side main pane occupies (XmonadTall)'
        ),
    Key([mod], "space",
        lazy.layout.next(),
        desc='Switch window focus to other pane(s) of stack'
        ),
    Key([mod, "control"], "Return",
        lazy.layout.toggle_split(),
        desc='Toggle between split and unsplit sides of stack'
        ),
]

#GROUPS
groups = [Group(i) for i in "1234"]

for i in groups:
    keys.extend([
        # mod1 + letter of group = switch to group
        Key([mod], i.name, lazy.group[i.name].toscreen()),

        # mod1 + shift + letter of group = switch to & move focused window to group
        Key([mod, "shift"], i.name, lazy.window.togroup(i.name, switch_group=True)),
        # Or, use below if you prefer not to switch to that group.
        # # mod1 + shift + letter of group = move focused window to group
        # Key([mod, "shift"], i.name, lazy.window.togroup(i.name)),
    ])

#LAYOUTS
layout_theme = {"border_width": 2,
                "margin": 6,
                "border_focus": "e1acff",
                "border_normal": "1D2330"
                }

layouts = [
    # layout.Bsp(),
    # layout.Columns(),
    # layout.Matrix(),
    layout.MonadTall(**layout_theme),
    layout.Floating(**layout_theme),
    layout.Max(**layout_theme),
    layout.Stack(num_stacks=2, **layout_theme),
    layout.TreeTab(**layout_theme),
    # layout.MonadWide(),
    # layout.RatioTile(),
    # layout.Tile(),
    # layout.VerticalTile(),
    # layout.Zoomy(),
]

#COLORS
colors = [["#292d3e", "#292d3e"], # panel background
          ["#434758", "#434758"], # background for current screen tab
          ["#ffffff", "#ffffff"], # font color for group names
          ["#ff5555", "#ff5555"], # border line color for current tab
          ["#8d62a9", "#8d62a9"], # border line color for other tab and odd widgets
          ["#668bd7", "#668bd7"], # color for the even widgets
          ["#e1acff", "#e1acff"]] # window name

### Mouse Callbacks ###
def dec_bright(qtile):
    qtile.cmd_spawn('alacritty -e xbacklight -inc 5')

def pacman_update(qtile):
    qtile.cmd_spawn('alacritty -e sudo pacman -Syu')


### WIDGETS
widget_defaults = dict(
    font='Ubuntu Mono',
    fontsize=12,
    padding=3,
    background = colors[0]
)
extension_defaults = widget_defaults.copy()

screens = [
    Screen(
        top=bar.Bar(
            [
		widget.GroupBox(active = colors[2], inactive = colors[2], rounded = False, highlight_color = colors[1], highlight_method = "line", foreground = colors[2], background = colors[0]),
                widget.Prompt(),
                widget.WindowName(),
		widget.TextBox(text = '◀', background = colors[0], foreground = colors[5], padding = 0, fontsize = 37),
		widget.Net(interface = 'wlp1s0', format = '{down}  ↓↑ {up}', foreground = colors[2], background = colors[5]),
                widget.TextBox(text = '◀', foreground = colors[4], background = colors[5], padding = 0, fontsize = 37),
		widget.TextBox(text = "⟳", padding = 2, foreground = colors[2], background = colors[4], fontsize = 14),
		widget.Pacman(update_interval = 1800, foreground = colors[2], background = colors[4]),
                widget.TextBox(text = "Updates", padding = 5, foreground = colors[2], background = colors[4], mouse_callbacks = {'Button1': pacman_update}),
                widget.TextBox(text = '◀', background = colors[4], foreground = colors[5], padding = 0, fontsize = 37),
                widget.CurrentLayoutIcon(custom_icon_paths = [os.path.expanduser("~/.config/qtile/icons")], foreground = colors[2], background = colors[5], padding = 0, scale = 0.7),
		widget.CurrentLayout(foreground = colors[2], background = colors[5], padding = 5),
		widget.TextBox(text = '◀', background = colors[5], foreground = colors[4], padding = 0, fontsize = 37),
		widget.TextBox(text = "Br:", foreground = colors[2], background = colors[4], padding = 0),
		widget.Backlight(backlight_name = 'ePD1', brightness_file = '/sys/class/backlight/intel_backlight/brightness', max_brightness_file = '/sys/class/backlight/intel_backlight/max_brightness', foreground = colors[2], background = colors[4], padding = 5, mouse_callbacks = {'Button1': dec_bright}),
		widget.TextBox(text = '◀', background = colors[4], foreground = colors[5], padding = 0, fontsize = 37),
		widget.TextBox(text = "Vol:", foreground = colors[2], background = colors[5], padding = 0),
		widget.Volume(foreground = colors[2], background = colors[5], padding = 5),
		widget.TextBox(text = '◀', background = colors[5], foreground = colors[4], padding = 0, fontsize = 37),
                widget.Battery(format = '{char} {percent:2.0%}', foreground = colors[2], background = colors[4], padding = 5),
		widget.TextBox(text = '◀', background = colors[4], foreground = colors[5], padding = 0, fontsize = 37),
                widget.Clock(format='%d/%m/%Y %a [ %H:%M ]', foreground = colors[2], background = colors[5]),
                widget.Systray(),
            ],
            24,
        ),
    ),
]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
main = None
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    {'wmclass': 'confirm'},
    {'wmclass': 'dialog'},
    {'wmclass': 'download'},
    {'wmclass': 'error'},
    {'wmclass': 'file_progress'},
    {'wmclass': 'notification'},
    {'wmclass': 'splash'},
    {'wmclass': 'toolbar'},
    {'wmclass': 'confirmreset'},  # gitk
    {'wmclass': 'makebranch'},  # gitk
    {'wmclass': 'maketag'},  # gitk
    {'wname': 'branchdialog'},  # gitk
    {'wname': 'pinentry'},  # GPG key password entry
    {'wmclass': 'ssh-askpass'},  # ssh-askpass
])
auto_fullscreen = True
focus_on_window_activation = "smart"

@hook.subscribe.startup_once
def autostart():
    home = os.path.expanduser('~')
    subprocess.call([home + '/.config/qtile/autostart.sh'])

@hook.subscribe.startup_once
def autostart():
    home = os.path.expanduser('~')
    subprocess.call([home + '/.screenlayout/screen_res.sh'])

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
